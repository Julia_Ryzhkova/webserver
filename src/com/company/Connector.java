package com.company;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by ���� on 12.02.2016.
 */
public class Connector {
    private static final String url = "jdbc:mysql://localhost:3306/first_database";
    private static final String user = "root";
    private static final String password = "";

    private Connection connect;

    public Connector() throws SQLException {
    }

    public Connection open() throws SQLException {
        // ����������� ��������
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        // opening database connection to MySQL server
        connect = DriverManager.getConnection(url, user, password);
        return connect;
    }

    public void close() throws SQLException {
        if (connect != null)
        {
            connect.close();
        }
    }
}
