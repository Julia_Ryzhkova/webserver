package com.company;

import java.util.Map;

/**
 * Created by ���� on 19.01.2016.
 */
public interface Page {
    String getUri();

    String getContent(Map<String, Object> request);

}
