package com.company;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Dao {

    private static final String url = "jdbc:mysql://localhost:3306/first_database";
    private static final String user = "root";
    private static final String password = "";

    private static Connection con;
    private static Statement stmt;
    private static ResultSet rs;

    public List<Person> getPeople() {

        List<Person> people = new ArrayList<>();

//        people.add(new Person("Julia"));
//        people.add(new Person("Julia 1"));
//        people.add(new Person("Julia 2"));
//        people.add(new Person("Julia 3"));
//        people.add(new Person("Julia 4"));

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        String query = "select name, second_name, phone from friend";

        try {
            // opening database connection to MySQL server
            con = DriverManager.getConnection(url, user, password);

            // getting Statement object to execute query
            stmt = con.createStatement();

            // executing SELECT query
            rs = stmt.executeQuery(query);

            while (rs.next()) {
//                int count = rs.getInt(1);
                Person person = new Person();

                person.setName(rs.getString(1));

                String second_name = rs.getString(2);
                person.setSecondName(second_name);

                String phone = rs.getString(3);
                person.setPhone(phone);

                people.add(person);

//                System.out.println(person.getName() + " " + second_name + " " + phone);
            }

        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        } finally {
            //close connection ,stmt and result set here
            try { con.close(); } catch(SQLException se) { /*can't do anything */ }
            try { stmt.close(); } catch(SQLException se) { /*can't do anything */ }
            try { rs.close(); } catch(SQLException se) { /*can't do anything */ }
        }

        return people;
    }
    public static void  addPeople(String name, String phone) {
        Connector connection = null;
        try {
            connection = Application.getConnection();

            Statement statement = connection.open().createStatement();

            String query = String.format("insert into friend (name, phone) values('%s', '%s')", name, phone);

            statement.executeUpdate(query);

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null)
            {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
