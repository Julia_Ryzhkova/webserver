package com.company;

import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by ���� on 19.01.2016.
 */
public class WebServer {
    List<Page> pages = new ArrayList<>();

    public List<Page> getPages() {
        return pages;
    }

    public void run() {
        ExecutorService executor = Executors.newCachedThreadPool();

        try {
            ServerSocket serverSocket = new ServerSocket(8080);
            while (true) {

                Socket socket = serverSocket.accept();

                executor.submit(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            byte[] mass = new byte[1];

                            ArrayList<Byte> buff = new ArrayList<>();
                            Boolean stopRead = false;
                            StringBuffer str = new StringBuffer();

                            while (!stopRead) {

                                socket.getInputStream().read(mass);

                                buff.add(mass[0]);
                                str.append((char) mass[0]);


                                if (buff.size() > 3) {
                                    if (buff.get(buff.size() - 1) == 10) {
                                        if (buff.get(buff.size() - 2) == 13 &&
                                                buff.get(buff.size() - 3) == 10 &&
                                                buff.get(buff.size() - 4) == 13) {
                                            stopRead = true;
                                        }
                                    }
                                }
                                if (mass[0] == 0) {
                                    stopRead = true;
                                }
                            }


                            String uri = "";
                            String getParamsStr = "";




                            String[] getParamsMass = {};
                            if (buff.size() > 2) {
                                String[] getRequestStrs = str.toString().split("\r\n")[0].split(" ")[1].split("\\?");

                                uri = getRequestStrs[0];

                                if (getRequestStrs.length > 1) {
                                    getParamsStr = getRequestStrs[1];
                                    getParamsMass = getParamsStr.split("\\&");
                                    Map<String, String> getParams = new HashMap<>();
                                    for (int i = 0; i < getParamsMass.length; i++) {
                                        String[] splMass = getParamsMass[i].split("=");
                                        String key = splMass[0];
                                        String value = "";
                                        if (splMass.length > 1) {
                                            value = splMass[1];
                                        }
                                        getParams.put(key, value);
                                    }
                                }


                            }


                            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(socket.getOutputStream(), "utf8");


                            String html = "404";
                            /*if (uri.equals("/")) {
                                html = readFileAsString("html.html");
                            } else if (uri.equals("/formSubmit")) {
                                html = getParams.get("userName");
                            }*/
                            for (Page page: pages) {
                              if (uri.equals(page.getUri())) {
                                  Map<String, Object> request = new HashMap<String, Object>();
                                  request.put("raw", buff);
                                  request.put("getParams", getParamsMass);
                                  html = page.getContent(request);
                              }
                            }



                            outputStreamWriter.write("HTTP/1.1 200 OK\n");
                            outputStreamWriter.write("Content-Type: text/html; charset=utf-8\n");
                            outputStreamWriter.write("Connection: close\n");
                            outputStreamWriter.write("Content-Length: " + html.getBytes("UTF-8").length + "\n");
                            outputStreamWriter.write("\n");

                            outputStreamWriter.write(html);

                            outputStreamWriter.flush();

                        } catch (Exception r) {
                            r.printStackTrace();
                        }
                    }
                });

            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
    private static String readFileAsString(String filePath) throws IOException {
        return new Scanner(new File(filePath), "UTF-8").useDelimiter("\\Z").next();
    }
}
