package com.company;

import java.sql.*;
import java.util.*;

public class Main {


    public static void main(String[] args) {
        WebServer webServer = new WebServer();

        Dao dao = new Dao();



        webServer.getPages().add(new Page() {
            @Override
            public String getUri() {
                return "/";
            }

            @Override
            public String getContent(Map<String, Object> request) {
                List<Person> peopleList = dao.getPeople();
                String content;

                content = "<ul>";
                    for (Person people : peopleList) {
                        content += "<li>" + people.getName() + " " + people.getSecondName() + " " + people.getPhone() + "</li>";
                    }
                content += "</ul>";


                return "<html>" +
                            "<body>" +
                                content +
                            "</body>" +
                        "</html>";
            }
        });

        webServer.getPages().add(new Page(){

            @Override
            public String getUri() {
                return "/add/";
            }

            @Override
            public String getContent(Map<String, Object> request) {
                String[] params = (String[]) request.get("getParams");

                Map<String, String> getParamsRequest = new HashMap<>();
                if (params.length > 1) {
                    for (int i = 0; i < params.length; i++) {
                        String[] getParamsRequestSplit = params[i].split("=");
                        String key = getParamsRequestSplit[0];
                        String value = "";
                        if (getParamsRequestSplit.length > 1) {
                            value = getParamsRequestSplit[1];
                        }
                        getParamsRequest.put(key, value);
                    }
                }
                if (getParamsRequest.get("name").length() > 0 &&
                    getParamsRequest.get("phone").length() > 0) {
                    dao.addPeople(getParamsRequest.get("name"), getParamsRequest.get("phone"));
                }

//                System.out.println(params.length);
                return "<html>" +
                            "<body>" +
                                "<form method='get'>" +
                                    "<input type='text' name='name'><br/>" +
                                    "<input type='text' name='phone'><br/>" +
                                    "<button type='submit'>Добавить</button>" +
                                "</form>" +
                            "</body>" +
                        "</html>";
            }
        });

        webServer.run();
    }

}
